from lxml import etree

f1 = './running-config.xml'
f2 = './candidate-config.xml'
f3 = './changed-config.xml'

if __name__ == '__main__':
    rconf_etree = etree.parse(f1)
    cconf_etree = etree.parse(f2)
    changed_etree = etree.parse(f3)

    rconf_etree = etree.ElementTree(
        rconf_etree.xpath('/response/result/config').pop())

    cconf_etree = etree.ElementTree(
        cconf_etree.xpath('/response/result/config').pop())

    changed_root_etree = etree.ElementTree(
        changed_etree.xpath('/response/result/devices').pop())

    change_stack = list()

    for e in rconf_etree.iter():
        rconf_path = rconf_etree.getpath(e)
        try:
            cconf_element = cconf_etree.xpath(rconf_path).pop()
            if etree.tostring(etree.ElementTree(e)).rstrip().lstrip() != etree.tostring(etree.ElementTree(cconf_element)).rstrip().lstrip():
                addflag = True
                for xpath in change_stack:
                    if rconf_path.startswith(xpath):
                        addflag = False
                if addflag:
                    change_stack.append(rconf_path)
        except BaseException:
            pass

    for xpath in change_stack:
        dirty_elements = changed_root_etree.xpath('//*[@dirtyId]')
        lowest_level_dirty = []
        for e in dirty_elements:
            dirty_subset = []
            subtree = etree.ElementTree(e)
            clean_subset = subtree.xpath('//*[not(@dirtyId)]')
            if len(clean_subset) == 0:
                addflag = True
                for item in lowest_level_dirty:
                    if changed_root_etree.getpath(e).startswith(changed_root_etree.getpath(item)):
                        addflag = False
                if addflag:
                    lowest_level_dirty.append(e)
        for e in lowest_level_dirty:
            xpath = '/config'+changed_root_etree.getpath(e)
            cconf_element = cconf_etree.xpath(xpath).pop()
            print("Changed XPATH: {} modified by user {} at {}\n".format(
                xpath, e.attrib['admin'], e.attrib['time']))
            print(etree.tostring(cconf_element).decode('utf-8'))
